// import { initializeApp } from 'firebase/app';
// import {
//   onAuthStateChanged,
//   signInWithEmailAndPassword,
//   getAuth
// } from 'firebase/auth';
import {initializeApp} from 'https://www.gstatic.com/firebasejs/9.4.0/firebase-app.js';
import {
  onAuthStateChanged,
  signInWithEmailAndPassword,
  getAuth
} from 'https://www.gstatic.com/firebasejs/9.4.0/firebase-auth.js';
const firebaseConfig = {
  apiKey: "AIzaSyAJ4e1jF_xIrFx3Qtn0Cw_UUtP8UqCchDc",
  authDomain: "gbassan-project-343121.firebaseapp.com"
};
const app = initializeApp(firebaseConfig);
const auth = getAuth(app, {/* extra options */ });
const tenant = "app1-dumq3"
auth.tenantId = tenant
console.log(auth)
document.addEventListener("DOMContentLoaded", () => {
  onAuthStateChanged(auth, (user) => {
      
      if (user) {
          console.log(user)
          document.getElementById("message").innerHTML = "Welcome, " + user.email;
      }
      else {
          document.getElementById("message").innerHTML = "No user signed in.";
      }
  });
  signIn();
});

function signIn() {
  const email = "gbassan@outlook.com";
  const password = "Passw0rd";
  signInWithEmailAndPassword(auth, email, password)
      //console.log(auth)
      .catch((error) => {
          document.getElementById("error").innerHTML = error.message;
      });
}