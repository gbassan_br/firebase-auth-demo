
import {initializeApp} from 'https://www.gstatic.com/firebasejs/9.4.0/firebase-app.js';
import { RecaptchaVerifier, getAuth, signInWithPhoneNumber, PhoneAuthProvider, getIdToken } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-auth.js";
//import { getFirestore } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
//importar o SDK do Firebase Admin para verificar o token
//const { initializeApp } = require('firebase-admin/app'); 
// Turn off phone auth app verification.
//app.settings.appVerificationDisabledForTesting = true;

// var phoneNumber = "+5511960601685";
// var testVerificationCode = "123456";
const firebaseConfig = {
    apiKey: "AIzaSyAJ4e1jF_xIrFx3Qtn0Cw_UUtP8UqCchDc",
    authDomain: "gbassan-project-343121.firebaseapp.com",
    projectId: "gbassan-project-343121",
    storageBucket: "gbassan-project-343121.appspot.com",
    messagingSenderId: "1077010764940",
    appId: "1:1077010764940:web:bc461d6f1673e40da4d7a9"
  };
const app = initializeApp(firebaseConfig);

// This will render a fake reCAPTCHA as appVerificationDisabledForTesting is true.
// This will resolve after rendering without app verification.
const auth = getAuth()
//const db = getFirestore()
auth.settings.appVerificationDisabledForTesting = false;
//const tenant = "app1-dumq3"
//auth.tenantId = tenant
//console.log(db)
console.log(auth)
var appVerifier = new RecaptchaVerifier('recaptcha-container', {}, auth);
// signInWithPhoneNumber will call appVerifier.verify() which will resolve with a fake
// reCAPTCHA response.
var phone = window.prompt('Digite seu número de celular')
//signInWithPhoneNumber(auth, phoneNumber, appVerifier)
console.log(phone)
signInWithPhoneNumber(auth, phone, appVerifier)
    .then(function (confirmationResult) {
      // confirmationResult can resolve with the fictional testVerificationCode above.
      console.log(confirmationResult)
    //   var credential = PhoneAuthProvider.credential(confirmationResult.verificationId, testVerificationCode)
    //   console.log(credential)
    //   var login =  confirmationResult.confirm(testVerificationCode)
      var code = window.prompt('Digite o código recebido aqui')
      // possível fazer login com uso das credentials
      var credential = PhoneAuthProvider.credential(confirmationResult.verificationId, code)
      console.log(credential)
      var login =  confirmationResult.confirm(code)
      console.log(login)
      auth.currentUser.getIdToken(/* forceRefresh */ true)
        .then(function(idToken) {
        // Send token to your backend via HTTPS
        console.log(idToken)
        // ...
      }).catch(function(error) {
        // Handle error
      });
      return
    }).catch(function (error) {
        console.log(error)
      // Error; SMS not sent
      // ...
    });
// var provider = new PhoneAuthProvider();
// var phone = window.prompt('bota teu numero ai')
// provider.verifyPhoneNumber(auth, phone, appVerifier)
//   .then(function(verificationId){
//     var verificationCode = window.prompt('Please enter the verification ' +
//     'code that was sent to your mobile device.');
//     return PhoneAuthProvider.credential(verificationId, verificationCode);
//   })